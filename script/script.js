
function showRecord() {


	pageNo = 1
	var count = parseInt(document.getElementById('show-record').value)
	pageSize = count;
	pageCount = Math.ceil(recordCount / pageSize)


	calculatePagination()
}

var pageSize = 2
var pageNo = 2
var pageCount = 0
var recordCount = 0



function first() {

	if (pageNo == 1) { }
	else {
		pageNo = 1
		calculatePagination()
	}

}
function last() {

	if (pageNo == pageCount) { } else {
		pageNo = pageCount
		calculatePagination()
	}

}
function pre() {

	if (pageNo > 1) {
		pageNo = pageNo - 1
		calculatePagination()
	}
}
function next() {

	if (pageNo + 1 <= pageCount) {
		pageNo = pageNo + 1
		calculatePagination()
	}
}

function calculatePagination() {


	var start = pageNo * pageSize - pageSize;
	var end = pageNo * pageSize

	var pageNoArray = ticketList.slice(start, end)
	RenderGrid(pageNoArray);
}




var ticketList = []
function sort(e) {


	var key = e.getAttribute('data-key')
	var order = e.getAttribute('data-order')

	if (order == 'ascending') {
		e.setAttribute('data-order', 'descending')
	} else {
		e.setAttribute('data-order', 'ascending')
	}


	var sortedList = ticketList.sort(function (a, b) {
		var aKey = a[key]
		var bKey = b[key]

		let comparison = 0;
		if (aKey > bKey) {
			if (order == 'ascending') { comparison = 1 }
			else { comparison = -1; }
		} else if (aKey < bKey) {
			if (order == 'ascending') { comparison = -1 }
			else { comparison = 1; }
		}
		return comparison;
	})

	RenderGrid(sortedList)
}

function RenderGrid(tL) {

	var option = tL.map(function (x) {
		return `<tr>
				<td>${x.CustomerName}</td>
				<td>${x.ContractNumber}</td>
				<td>${x.CustomerId}</td>
				<td>${x.TicketChannelName}</td>
				<td>${x.ContactNo}</td>
				<td>${x.Email}</td>
				<td>${x.TicketTypeName}</td>
				<td>${x.TicketAssignGroupName}</td>
				<td>${x.AssignedTo}</td>
				<td>${x.PriorityName}</td>
				<td>${x.GroupName}</td>
				<td>${x.SubGroupName}</td>
				<td></td>
				<td></td>             					
			</tr>`;
	});
	var tableHTML = option.join("");
	$("table tbody").empty();
	$("table tbody").append(tableHTML);
}

showData()


function showData() {

	$.ajax({
		type: "GET",
		url: "https://erp.arco.sa:65//api/GetMyTicket?CustomerId=CIN0000150",
		success: function (data) {

			ticketList = data.Data
			recordCount = data.Data.length
			pageCount = Math.ceil(recordCount / pageSize)
			document.getElementById('show-record').value = pageSize;
			calculatePagination()
		}
	})
}

function loadCustomerDropList() {

	$.ajax({
		type: "GET",
		url: "https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150",
		success: function (data) {

			var allOption = data.Result1.map(function (x) {
				return `<option value="${x.CustomerID}" >${x.FirstName} ${x.MiddleName} ${x.LastName}</option>`
			})

			document.getElementById("cName").innerHTML = allOption.join("");


		}
	});

}

loadCustomerDropList()
loadContractNumberList()

var contractValue = [];

function contractPart() {

	document.getElementById("cNum").style.display = "none";

	var store = document.getElementById("my_input").value;
	if (store) {
		document.getElementById("cNum").style.display = "block";

		var inputValue = contractValue.filter(function (x) {
			return x.ContractNumber.toLowerCase().includes(store.toLowerCase());
		})

		var storeValue = inputValue.map(function (y) {
			return `<div  onclick ="onContractSelect('${y.ContractNumber}','${y.CustomerID}')">${y.ContractNumber}</div>`
		}).join("");

		document.getElementById("cNum").innerHTML = storeValue;

	} else {
		document.getElementById("cNum").style.display = "none";
		document.getElementById("cNum").innerHTML = null;
	}

}

function onContractSelect(name, id) {

	var item = document.getElementById('my_input')
	item.value = name
	item.setAttribute("data-value", name)


	document.getElementById("cNum").style.display = "none";
	document.getElementById("cNum").innerHTML = null;
	loadLabourIDList()

}

function loadContractNumberList() {
	$.ajax({
		type: "GET",
		url: "https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150",
		success: function (data) {
			console.log("table", data)
			contractValue = data.Result2;
			var allOption = data.Result2.map(function (x) {
				return `<option  value="${x.ContractNumber}">${x.ContractNumber}</option>`
			})
			document.getElementById("cNum").innerHTML = allOption.join("");
		}
	})

}

function loadLabourIDList() {
	debugger

	var cid = document.getElementById('my_input').getAttribute('data-value');

	$.ajax({
		type: "GET",
		url: " https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=" + cid,
		success: function (data) {

			var allOption = data.map(function (x) {

				return `<option value="${x.EmployeeId}">${x.Name}</option>`
			})
			document.getElementById("lID").innerHTML = allOption.join("");
		}
	})

}



loadTicketTypeList()

function loadTicketTypeList() {

	$.ajax({
		type: "GET",
		url: "https://erp.arco.sa:65//api/TickettypeList",
		success: function (data) {


			var allOption = data.Data.map(function (x) {
				return `<option value="${x.ID}">${x.Name}</option>`
			})
			document.getElementById("tType").innerHTML = allOption.join("");
		},



	})
}



function loadDepartmentList() {


	var dep = document.getElementById("tType").value;

	$.ajax({
		type: "GET",
		url: "https://erp.arco.sa:65/api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=" + dep,
		success: function (data) {



			var allOption = data.map(function (x) {
				return `<option value="${x.ID}">${x.Name}</option>`
			})

			document.getElementById("dep").innerHTML = allOption.join("");

		}
	})

}

var getValue = [];

function storeValue() {

	document.getElementById("assignTo").style.display = "none";

	var store = document.getElementById("myInput").value;
	if (store) {
		document.getElementById("assignTo").style.display = "block";

		var inputValue = getValue.filter(function (x) {
			return x.Name.toLowerCase().includes(store.toLowerCase());
		})

		var storeValue = inputValue.map(function (y) {
			return `<div onclick="onAssignToSelect('${y.Name}', '${y.ID}')" >${y.Name}</div>`
		}).join("");

		document.getElementById("assignTo").innerHTML = storeValue;

	} else {
		document.getElementById("assignTo").style.display = "none";
		document.getElementById("assignTo").innerHTML = null;
	}

}


function onAssignToSelect(name, id) {

	var item = document.getElementById('myInput')
	item.value = name
	item.setAttribute("data-value", id)


	document.getElementById("assignTo").style.display = "none";
	document.getElementById("assignTo").innerHTML = null;

}

function loadAssignedToList() {

	var assign = document.getElementById("dep").value;
	$.ajax({

		type: "GET",
		url: " https://erp.arco.sa:65//api/assigntoList",
		success: function (data) {
			console.log(data);
			getValue = data.Data;
			var allOption = data.Data.map(function (x) {
				return `<option  value="${x.ID}">${x.Name}</option>`
			})
			document.getElementById("assignTo").innerHTML = allOption.join("");
		}
	})
}
loadPriorityList()
function loadPriorityList() {
	$.ajax({
		type: 'GET',
		url: "https://erp.arco.sa:65//api/PriorityList",
		success: function (subject) {
			var allOption = subject.Data.map(function (y) {
				return `<option>${y.Name}</option>`
			})
			document.getElementById("Priority").innerHTML = allOption.join("");
		}
	})
}

function loadcategoryList() {

	var cat = document.getElementById("dep").value;
	$.ajax({
		type: "GET",
		url: "https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=" + cat,
		success: function (data) {

			var allOption = data.map(function (x) {
				return `<option value="${x.TicketGroupID}">${x.TicketGroupName}</option>`
			})
			document.getElementById("category").innerHTML = allOption.join("");
		},
		error: function (error) {


		}
	})
}

function loadSubCategoryList() {

	var sub = document.getElementById("category").value;

	$.ajax({

		type: "GET",
		url: " https://erp.arco.sa:65/api/SubGroupByGroup?id=" + sub,
		success: function (data) {
			var allOption = data.map(function (x) {
				return `<option>${x.Description}</option>`
			})
			document.getElementById("subCategory").innerHTML = allOption.join("");
		}
	})
}

function create() {


	var popupvalid = true;

	var total = {
		customerName: document.getElementById("cName").value,
		contractNumber: document.getElementById("cNum").value,
		labourID: document.getElementById("lID").value,
		ticketChannel: document.getElementById("tChannel").value,
		Contact: document.getElementById("contact").value,
		Email: document.getElementById("Email").value,
		ticketType: document.getElementById("tType").value,
		Department: document.getElementById("dep").value,
		assignTo: document.getElementById("assignTo").value,
		Priority: document.getElementById("Priority").value,
		Category: document.getElementById("category").value,
		subCategory: document.getElementById("subCategory").value,
		Subject: document.getElementById("subject").value,
		Description: document.getElementById("desc").value,
		file: document.getElementById("file").value,
	}





	if (total.customerName == "" || total.customerName == null) {
		alert("please fil out the field");
	}

	if (total.contractNumber == "" || total.contractNumber == null) {
		alert("please fil out the field");
	}

	if (total.ticketChannel == "" || total.ticketChannel == null) {
		alert("please fil out the field");
	}

	if (total.Contact == "" || total.Contact == null) {
		alert("please fil out the field");
		popupvalid = false;
	}

	if (total.Email == "" || total.Email == null) {
		alert("please fil out the field");
		popupvalid = false;
	}

	if (total.ticketType == "" || total.ticketType == null) {
		alert("please fil out the field");
	}

	if (total.Priority == "" || total.Priority == null) {
		alert("please fil out the field");
	}

	if (popupvalid) {
		$("#mymodal").modal("hide");

		setTimeout(function () {
			alert(JSON.stringify(total, null, 3))
		}, 0)

		setInterval()

	}

}


function contact() {

	var Contact = document.getElementById("contact").value;

	if (Contact == "" || Contact == null) {
		document.getElementById("contact-error").innerHTML = "<div style='color:orange'>please fill out this field</div>";
	} else {
		document.getElementById("contact-error").innerHTML = null;
	}
}

function customer() {
	var customerName = document.getElementById("cName").value;

	if (customerName == "" || customerName == null) {
		document.getElementById("customer-error").innerHTML = "<div style='color:orange'>please fill out this field</div>";
	} else {
		document.getElementById("customer-error").innerHTML = null;
	}
}

function contract() {

	var contractNumber = document.getElementById("cNum").value;

	if (contractNumber == "" || contractNumber == null) {
		document.getElementById("contract-error").innerHTML = "<div style='color:orange'>please fill out this field</div>";
	} else {
		document.getElementById("contract-error").innerHTML = null;
	}
}

function ticketChannel() {
	var ticketChannel = document.getElementById("tChannel").value;

	if (ticketChannel == "" || ticketChannel == null) {
		document.getElementById("ticketchannel-error").innerHTML = "<div style='color:orange'>please fill out this field</div>";
	} else {
		document.getElementById("ticketchannel-error").innerHTML = null;
	}
}

function Email() {
	var email = document.getElementById("Email").value;

	if (email == "" || email == null) {
		document.getElementById("Email-error").innerHTML = "<div style='color:orange'>please fill out this field</div>";
	}
	else if (!email.includes("@")) {
		document.getElementById("Email-error").innerHTML = "<div style='color:orange'>please input valid Email ID</div>";
	} else {
		document.getElementById("Email-error").innerHTML = null;
	}
}


function ticketType() {
	var ticketType = document.getElementById("tType").value;

	if (ticketType == "" || ticketType == null) {
		document.getElementById("tickettype-error").innerHTML = "<div style='color:orange'>please input valid Email ID</div>";
	} else {
		document.getElementById("tickettype-error").innerHTML = null;
	}
}

function Priority() {
	var Priority = document.getElementById("Priority").value;

	if (Priority == "" || Priority == null) {
		document.getElementById("priority-error").innerHTML = "<div style='color:orange'>please input valid Email ID</div>";
	} else {
		document.getElementById("priority-error").innerHTML = null;
	}
}

function filevalue() {
	var file = document.getElementById("file").value;

	if (file == "" || file == null) {
		document.getElementById("file-error").innerHTML = "<div style='color:orange'>please input valid Email ID</div>";
	} else {
		document.getElementById("file-error").innerHTML = null;
	}
}




























